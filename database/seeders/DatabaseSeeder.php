<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Category::create([
        //      'title'=> 'Women',
        //    'description'=> 'This is test description'
        //  ]);

        //  Category::create([
        //     'title'=> 'Men',
        //     'description'=> 'This is test description'
        // ]);

        // Category::create([
        //     'title'=> 'Kids',
        //   'description'=> 'This is test description'
        // ]);
        Category::create([
            'title'=> 'Kids',
          'description'=> 'This is test description',
          'is_active'=> 1
        ]);
        Category::create([
            'title'=> 'Men',
          'description'=> 'This is test1 description',
          'is_active'=>1
        ]);
        Category::create([
            'title'=> 'Women',
          'description'=> 'This is test2 description',
          'is_active'=>0
        ]);



        
        // echo 'seeding data';
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
