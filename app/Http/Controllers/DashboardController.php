<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function admin()
    {
       return view ('components.backend.partials.content');
    }

    public function table()
    {
       return view ('components.backend.common.table');
    }

    
}
