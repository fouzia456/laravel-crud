<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index()
    {
        $categoryAll = Category::all();
        // dd($categories);
       return view ('components.backend.common.categories',[
        'categories' => $categoryAll
       ]);
    }

    public function create()
    {
        return view('components.backend.common.create');
    }

    public function store(CategoryRequest $request)
    {
        //validation

        
            // $validator = Validator::make($request->all(), [
            //     'title' => 'required|unique:posts|max:255|min:3',
            //     'description' => 'min:10|max:1000',
            // ]);
     
            // if ($validator->fails()) {
                
            //     return redirect()->back()
            //                 ->withErrors($validator)
            //                 ->withInput();
            // }
        //  $request->validate([
        //     'title' => 'required|unique:categories|max:255|min:3',
        //     'description' => 'nullable|min:10|max:1000',
        // ]);
    //    dd('passed');
        // return $request->all();
        Category::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'is_active' => $request->is_active ? true : false,

        ]);

        // Session::flush('message','Successfully created');
        // Session::flash('message', 'Succesfully created');

        return redirect()->route('categories.index')->withMessage('Successfuly created');
    }

    public function show($categoryId)
    {
    //    $category = Category::where('id', '=', $categoryId)->first();
    $category = Category::findOrFail( $categoryId);
       return view('components.backend.common.show', compact('category'),[
        'category'=>$category
       ]);
        // dd('showing'.$categoryId);
    }

    public function edit($categoryId)
    {
        $category = Category::findOrFail( $categoryId);
        return view('components.backend.common.edit', compact('category'));
        // dd($category);
    }
    public function update(CategoryRequest $request,$categoryId)
    {
        $category = Category::findOrFail( $categoryId);
        $category->update([
            'title'=>$request->title,
            'description'=>$request->description,
            'is_active' => $request->is_active ? true : false,

        ]);
        return redirect()->route('categories.index')->withMessage('Successfuly updated');
    
        // dd($category);
    }

    public function destroy($categoryId)
    {
        $category = Category::findOrFail( $categoryId);
        $category->delete();  
        return redirect()->route('categories.index')->withMessage('Successfuly deleted');
    }
}
