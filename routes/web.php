<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('components.frontend.partials.content');
});

Route::get('/cart', function () {
    return view('components.frontend.common.cart');
});

Route::get('/checkout', function () {
    return view('components.frontend.common.checkout');
});

Route::get('/order', function () {
    return view('components.frontend.common.order');
});

Route::get('/product_simple', function () {
    return view('components.frontend.common.product-simple');
});

Route::get('/categories', function () {
    return view('components.frontend.common.shop-classic-filter');
});


Route::prefix('/admin')->group(function(){
Route::get('',[DashboardController::class,'admin']);
Route::get('/table',[DashboardController::class,'table']);
Route::get('/categories',[CategoryController::class,'index'])->name('categories.index');
Route::get('/categories/create',[CategoryController::class,'create'])->name('categories.create');
Route::post('/categories',[CategoryController::class,'store'])->name('categories.store');
Route::get('/categories/{category}',[CategoryController::class,'show'])->name('categories.show');
Route::get('/categories/{category}/edit',[CategoryController::class,'edit'])->name('categories.edit');
Route::patch('/categories/{category}/edit',[CategoryController::class,'update'])->name('categories.update');
Route::delete('/categories/{category}',[CategoryController::class,'destroy'])->name('categories.destroy');




// Route::resource('categories', CategoryController::class);
});
// Route::get('/admin', function () {
//     return view('components.backend.partials.content');
// });
// Route::get('/table', function () {
//     return view('components.backend.common.table');
// });

