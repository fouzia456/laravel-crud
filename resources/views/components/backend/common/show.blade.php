<x-backend.layout.master>

    <main>
                        <div class="container-fluid px-4">
                            <h1 class="mt-4">Category details</h1>
                            <ol class="breadcrumb mb-4">
                                <li class="breadcrumb-item"><a href="index.html">Category List</a></li>
                                <li class="breadcrumb-item active"><a class="btn btn-primary" href="{{route('categories.index')}}" role="button">List</a></li>
                            </ol>
                            <div class="card mb-4">
                                
                            </div>
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-table me-1"></i>
                                    DataTable Example
                                </div>
                                 
                                <div class="card-body">
                                    <table class="table">
                                        
                                            <tr>
                                                
                                                <th>Title</th>
                                               
                                                 <td>{{ $category->title }}</td>
                                               
                                            </tr>
                                            <tr>
                                                
                                                <th>Description</th>
                                               
                                                 <td>{{ $category->description }}</td>
                                               
                                            </tr>
                                            <tr>
                                                
                                                <th>Is Active</th>
                                               
                                                 <td>{{ $category->is_active ? 'Active' : 'In Active' }}</td>
                                               
                                            </tr>
                                         
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </main>
    </x-backend.layout.master>
    