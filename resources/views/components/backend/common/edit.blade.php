<x-backend.layout.master>


    <div class="container-fluid px-4">
        <h1 class="mt-4">Categories</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Category edit</a></li>
            <li class="breadcrumb-item active"><a class="btn btn-primary" href="{{ route('categories.index') }}" role="button">List</a></li>
        </ol></div>
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <form method="POST" action="{{ route('categories.update',['category'=>$category->id]) }}" style="padding: 50px">
        @csrf
        @method('patch')
        <div class="mb-3">
          <label for="titleinput" class="form-label">Title</label>
          <input name ="title" type="text" value= "{{ old('title',$category->title) }}" class="form-control @error('title') is-invalid @enderror" id="titleinput" >

          @error('title')

          <div class="form-text text-danger">{{$message}}</div>
        </div>

          @enderror
          

        <div class="mb-3">
            <label for="descriptioninput" class="form-label">Description</label>
            <textarea name="description"class="form-control @error('description') is-invalid @enderror" id="descriptioninput" >
              {{ old('description', $category->description) }}
            </textarea>
             @error('description')

          <div class="form-text text-danger">{{$message}}</div>
        </div>

          @enderror
          

          <div class="form-check">
            <input name ="is_active" class="form-check-input" type="checkbox" value="1" id="isActiveInput" checked>
            <label class="form-check-label" for="isActiveInput">
              Is Active
            </label>
          </div>
        
        <button type="submit" class="btn btn-primary mt-2">Submit</button>
      </form>

</x-backend.layout.master>