<x-backend.layout.master>

<main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Category</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Category List</a></li>
                            <li class="breadcrumb-item active"><a class="btn btn-primary" href="{{route('categories.create')}}" role="button">Add new</a></li>
                        </ol>
                        <div class="card mb-4">
                            
                        </div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                DataTable Example
                            </div>
                             @if(request()->session()->has('message'))
                            <div class="alert alert-primary" role="alert">
                                {{session('message')}}
                              </div>
                              @endif
                            <div class="card-body">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Title</th>
                                            <th>Is active ?</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach ($categories as $category)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $category->title }}</td>
                                            <td>{{ $category->is_active? 'Active' : 'In active' }}</td>
                                            <td>
                                                <a class="btn btn-sm btn-info" href="{{ route('categories.show',['category'=>$category->id]) }}">show</a>
                                                <a class="btn btn-sm btn-warning" href="{{ route('categories.edit',['category'=>$category->id]) }}">Edit</a>

                                                <form method="post" action="{{route('categories.destroy',['category'=> $category->id])}}" style="display: inline">
                                                    @csrf
                                                    @method('delete')

                                                    <button class ="btn btn-sm btn-danger" onclick="return confirm('Are you sure want to delete')">Delete</button>
                                                </form>
                                                
                                            </td>
                                        </tr>
                                     @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </main>
</x-backend.layout.master>
